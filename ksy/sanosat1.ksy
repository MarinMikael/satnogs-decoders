---
meta:
  id: sanosat1
  title: Sanosat-1 GFSK, RTTY, CW beacon + digi message decoder
  endian: le
#  2023-12-03, DL7NDR (extension for RTTY, CW and Digipeater)
doc: |
  :field callsign: sanosat1_telemetry.body.callsign
  :field packet_type: sanosat1_telemetry.body.packet_type
  :field com_temperature: sanosat1_telemetry.body.com_temperature
  :field battery_voltage: sanosat1_telemetry.body.battery_voltage
  :field charging_current: sanosat1_telemetry.body.charging_current
  :field battery_temperature: sanosat1_telemetry.body.battery_temperature
  :field radiation_level: sanosat1_telemetry.body.radiation_level
  :field no_of_resets: sanosat1_telemetry.body.no_of_resets
  :field antenna_deployment_status: sanosat1_telemetry.body.antenna_deployment_status
  :field checksum_rtty_hex: sanosat1_telemetry.body.checksum_rtty_hex
  :field digimessage: sanosat1_telemetry.body.decision.digimessage
  :field digimessage: sanosat1_telemetry.body.decision.digimessage
  :field cwmessage: sanosat1_telemetry.body.decision.cwmessage
  :field com_temperature: sanosat1_telemetry.body.decision.com_temperature
  :field battery_temperature: sanosat1_telemetry.body.decision.battery_temperature
  :field charging_current: sanosat1_telemetry.body.decision.charging_current
  :field battery_voltage: sanosat1_telemetry.body.decision.battery_voltage
  :field antenna_deployed: sanosat1_telemetry.body.decision.antenna_deployed
  :field checksum_cw_hex: sanosat1_telemetry.body.decision.checksum_cw_hex

seq:
  - id: sanosat1_telemetry
    type: sanosat1_telemetry_t

types:
  sanosat1_telemetry_t:
    seq:
      - id: body
        type:
          switch-on: check
          cases:
            0x4e394d410000ffff: with_delimiter
            0x010051504e394d41: without_delimiter
            0x242c51504e394d41: rtty # AM9NPQ,$
            0x242c71706e396d61: rtty # am9npq,$
            _: decision_digi_or_cw

    instances:
        check:
              type: u8
              pos: 0

  with_delimiter:
        seq:
            - id: delimiter
              type: s4
              valid:
                any-of:
                  - 0x0000ffff
            - id: callsign
              type: strz
              size: 7
              encoding: ASCII
              valid:
               any-of:
                 - '"AM9NPQ"'
            - id: packet_type
              type: s2
            - id: com_temperature
              type: s2
            - id: battery_voltage_hex_raw
              type: s2
              doc: '[/100 for V]'
            - id: charging_current
              type: s2
              doc: '[mA]'
            - id: battery_temperature
              type: s2
            - id: radiation_level
              type: s2
              doc: '[µSv/hr]'
            - id: no_of_resets
              type: s2
            - id: antenna_deployment_status
              type: u1

        instances:
            battery_voltage:
                 value: battery_voltage_hex_raw / 100.00

  without_delimiter:
         seq:
            - id: callsign
              type: strz # has to be strZ (=terminator 0), else ide.kaitai.io gripes
              size: 7
              encoding: ASCII
              valid:
               any-of:
                 - '"AM9NPQ"'
            - id: packet_type
              type: s2
            - id: com_temperature
              type: s2
            - id: battery_voltage_hex_raw
              type: s2
              doc: '[/100 for V]'
            - id: charging_current
              type: s2
              doc: '[mA]'
            - id: battery_temperature
              type: s2
            - id: radiation_level
              type: s2
              doc: '[µSv/hr]'
            - id: no_of_resets
              type: s2
            - id: antenna_deployment_status
              type: u1

         instances:
             battery_voltage:
                  value: battery_voltage_hex_raw / 100.00 # if last digit is "0", the last digit won't be displayed, despite of ".00"

  rtty:
         seq:
            - id: callsign
              type: str
              terminator: 0x2C
              encoding: ASCII
              valid:
               any-of:
                 - '"AM9NPQ"'
                 - '"am9npq"'
            - id: skip_dollar
              type: s1
            - id: battery_temperature_raw
              type: str
              terminator: 0x2C
              encoding: utf8
            - id: charging_current_raw
              type: str
              terminator: 0x2C
              encoding: utf8
            - id: battery_voltage_before_dot_raw
              type: str
              encoding: UTF-8
              terminator: 0x2E
            - id: battery_voltage_after_dot_raw
              type: str
              encoding: UTF-8
              terminator: 0x2C
            - id: no_of_resets_raw
              type: str
              terminator: 0x2C
              encoding: utf8
            - id: antenna_deployment_status_raw
              type: str
              terminator: 0x2C
              encoding: utf8
            - id: radiation_level_raw
              type: str
              terminator: 0x3F
              encoding: utf8
            - id: checksum_rtty_raw # NMEA checksum from battery_temperature to radiation_level
              size: 2
              type: str
              encoding: UTF-8

         instances:
               battery_temperature:
                 value: battery_temperature_raw.to_i
               charging_current:
                 value: charging_current_raw.to_i
               battery_voltage_before_dot:
                 value: battery_voltage_before_dot_raw.to_i
               battery_voltage_after_dot:
                 value: battery_voltage_after_dot_raw.to_i
               battery_voltage:
                 value: (battery_voltage_after_dot * 0.01) + battery_voltage_before_dot
               no_of_resets:
                 value: no_of_resets_raw.to_i
               antenna_deployment_status:
                 value: antenna_deployment_status_raw.to_i
               radiation_level:
                 value: radiation_level_raw.to_i
               checksum_rtty_hex:
                 value: checksum_rtty_raw.to_i(16)

  decision_digi_or_cw:
        seq:
            - id: decision
              type:
                switch-on: check
                cases:
                  0xffff: digi_with_delimiter
                  0x4d41: cw # AM
                  0x6d61: cw # am
                  _: digi_without_delimiter 

        instances:
             check:
                   type: u2
                   pos: 0


  digi_with_delimiter:
        seq:
            - id: delimiter
              type: s4
              valid:
                any-of:
                  - 0x0000ffff
            - id: header
              type: str
              size: 3
              encoding: ASCII
              valid:
               any-of:
                 - '"NPQ"'
            - id: digimessage
              type: str
              size-eos: true
              encoding: UTF-8

  digi_without_delimiter:
        seq:
            - id: header
              type: str
              size: 3
              encoding: ASCII
              valid:
               any-of:
                 - '"NPQ"'
            - id: digimessage
              type: str
              size-eos: true
              encoding: UTF-8

  cw:
        seq:
            - id: callsign
              type: str
              size: 6
              encoding: ASCII
              valid:
               any-of:
                 - '"AM9NPQ"'
                 - '"am9npq"'
            - id: cwmessage
              type: str
              size-eos: true
              encoding: UTF-8

        instances:
             length_of_cwmessage:
                   value: cwmessage.length
             residuals:
                   value: cwmessage.substring(length_of_cwmessage-5,length_of_cwmessage-3).to_i(16)
             length_of_bat_temp:
                   value: (residuals & 0b00000011) >> 0 # don't forget "(" ... ")"
             length_of_current:
                   value: (residuals & 0b00011100) >> 2
             sign_of_bat_temp:
                   value: '((residuals & 0b00100000) >> 5) == 0 ? "+" : "-"'
             sign_of_obc_temp:
                   value: '((residuals & 0b01000000) >> 6) == 0 ? "+" : "-"'
             antenna_deployed:
                   value: '((residuals & 0b10000000) >> 7) == 1 ? true : false'
             length_of_obc_temp: # results in the remaining digits of max. 9
                   value: length_of_cwmessage - 7 - length_of_current - length_of_bat_temp # "-7" = length of bat_temp (fix 2) + length of residuals (fix 2) + "?" + length of checksum (fix 2)
             com_temperature:
                   value: '(sign_of_obc_temp) == "-" ? (cwmessage.substring(0,length_of_obc_temp).to_i) * (-1) : cwmessage.substring(0,length_of_obc_temp).to_i'
             battery_temperature:
                   value: '(sign_of_bat_temp) == "-" ? (cwmessage.substring(length_of_obc_temp,length_of_obc_temp+length_of_bat_temp).to_i) * (-1) : cwmessage.substring(length_of_obc_temp,length_of_obc_temp+length_of_bat_temp).to_i'
             charging_current:
                   value: cwmessage.substring(length_of_obc_temp+length_of_bat_temp,length_of_obc_temp+length_of_bat_temp+length_of_current).to_i
             battery_voltage:
                   value: cwmessage.substring(length_of_obc_temp+length_of_bat_temp+length_of_current,length_of_obc_temp+length_of_bat_temp+length_of_current+2).to_i * 0.1
             checksum_cw_hex:
                   value: cwmessage.substring(length_of_cwmessage-2,length_of_cwmessage).to_i(16)
